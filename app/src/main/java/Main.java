import java.util.List;

import helpers.KoneksiDB;
import helpers.SetupDB;
import entities.Dosen;
import models.DosenDAO;
import models.DosenSQLite;
import entities.Mahasiswa;
import models.MahasiswaDAO;
import models.MahasiswaSQLite;
import java.sql.Connection;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args)  {
        SetupDB.migrate();
        
        
        MahasiswaDAO daoMhs = new MahasiswaSQLite();
        DosenDAO daoDosen = new DosenSQLite();

        Dosen[] dataDosen = {
            new Dosen("123456789", "James Gosling"),
            new Dosen("987654321", "Guido van Rossum")
        };

        for (Dosen dosen : dataDosen) { daoDosen.insert(dosen); }

        Mahasiswa[] dataMhs = {
            new Mahasiswa("A12345", "Abdul Lathif", 3.9, 120),
            new Mahasiswa("A23456", "Budi Jaya", 3.7, 110),
            new Mahasiswa("B12345", "Reni Wiyananti", 3.5, 130),
            new Mahasiswa("B23456", "Fajri Shubuh", 3.4, 135),
            new Mahasiswa("B34567", "Nur Luthfi", 3.5, 120),
        };

        dataMhs[0].nppDosen = dataDosen[0];
        dataMhs[1].nppDosen = dataDosen[1];
        dataMhs[2].nppDosen = dataDosen[0];
        dataMhs[4].nppDosen = dataDosen[1];

        for(Mahasiswa mhs : dataMhs) daoMhs.insert(mhs);

        List<Mahasiswa> listMhs = daoMhs.selectAll();
        listMhs.forEach(mhs -> System.out.println(mhs));

        dataMhs[3].ipk = 3.9;
        daoMhs.update(dataMhs[3]);

        dataMhs[1].nppDosen = dataDosen[0];
        daoMhs.update(dataMhs[1]);

        daoMhs.delete(dataMhs[0].nim);

        System.out.println("====================================");
        listMhs = daoMhs.selectAll();
        listMhs.forEach(mhs -> System.out.println(mhs));
        
    }
    
}
