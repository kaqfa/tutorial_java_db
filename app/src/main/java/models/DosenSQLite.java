package models;

import entities.Dosen;
import entities.Mahasiswa;
import helpers.KoneksiDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DosenSQLite implements DosenDAO {

    @Override
    public void insert(Dosen dosen) {
        String sql = "INSERT INTO dosen(npp, nama) VALUES(?, ?)";

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, dosen.npp);
            pstmt.setString(2, dosen.nama);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Dosen selectByNpp(String npp) {
        String sql = "SELECT npp, nama FROM dosen WHERE npp = ?";
        Dosen dosen = null;

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, npp);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                dosen = new Dosen(
                        rs.getString("npp"),
                        rs.getString("nama"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return dosen;
    }

    @Override
    public List<Dosen> selectAll() {
        String sql = "SELECT npp, nama FROM dosen";
        List<Dosen> dataDosen = new ArrayList<>();

        try (Connection conn = KoneksiDB.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {
                Dosen dosen = new Dosen(
                        rs.getString("npp"),
                        rs.getString("nama"));
                dataDosen.add(dosen);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return dataDosen;
    }

    @Override
    public void update(Dosen dosen) {
        String sql = "UPDATE dosen SET nama = ? WHERE npp = ?";

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, dosen.nama);
            pstmt.setString(2, dosen.npp);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void delete(String npp) {
        String sql = "DELETE FROM dosen WHERE npp = ?";

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, npp);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<Mahasiswa> getMahasiswaPerwalian(Dosen dosen) {
        String sql = "SELECT m.nim, m.nama, m.ipk, m.total_sks, d.npp, d.nama AS dosen_nama "
                   + "FROM mahasiswa m "
                   + "LEFT JOIN dosen d ON m.dosen_wali_npp = d.npp "
                   + "WHERE d.npp = ?";
        List<Mahasiswa> mahasiswaPerwalian = new ArrayList<>();

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, dosen.npp);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Mahasiswa mahasiswa = new Mahasiswa(
                        rs.getString("nim"),
                        rs.getString("nama"),
                        rs.getDouble("ipk"),
                        rs.getInt("total_sks"));
                mahasiswaPerwalian.add(mahasiswa);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return mahasiswaPerwalian;
    }
    
}
