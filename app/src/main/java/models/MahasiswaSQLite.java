package models;

import entities.Dosen;
import entities.Mahasiswa;
import helpers.KoneksiDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class MahasiswaSQLite implements MahasiswaDAO {

    @Override
    public void delete(String nim) {
        String sql = "DELETE FROM mahasiswa WHERE nim = ?";

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, nim);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void insert(Mahasiswa mahasiswa) {
        String sql = "insert into mahasiswa (nim, nama, ipk, total_sks) values (?, ?, ?, ?)";
        if(mahasiswa.nppDosen != null){
            sql = "insert into mahasiswa (nim, nama, ipk, total_sks, npp_dosen) values (?, ?, ?, ?, ?)";
        }
        
        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, mahasiswa.nim);
            pstmt.setString(2, mahasiswa.nama);
            pstmt.setDouble(3, mahasiswa.ipk);
            pstmt.setInt(4, mahasiswa.totalSks);
            if(mahasiswa.nppDosen != null){
                pstmt.setString(5, mahasiswa.nppDosen.npp);
            }
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Mahasiswa> selectAll() {
        String sql = """
            SELECT nim, mahasiswa.nama as nama, ipk, total_sks, 
                    dosen.npp, dosen.nama as nama_wali
                FROM mahasiswa 
                left join dosen on (dosen.npp = mahasiswa.npp_dosen)""";
        List<Mahasiswa> dataMhs = new ArrayList<>();

        try (Connection conn = KoneksiDB.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {
                Mahasiswa mahasiswa = new Mahasiswa(
                        rs.getString("nim"),
                        rs.getString("nama"),
                        rs.getDouble("ipk"),
                        rs.getInt("total_sks")
                );
                String npp_wali = rs.getString("npp");
                String nama_wali = rs.getString("nama_wali");
                if(npp_wali != null){
                    mahasiswa.nppDosen = new Dosen(npp_wali, nama_wali);
                }
                dataMhs.add(mahasiswa);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return dataMhs;
    }

    @Override
    public Mahasiswa selectByNim(String nim) {
        String sql = """
                SELECT nim, mahasiswa.nama as nama, ipk, total_sks, 
                        dosen.npp, dosen.nama as nama_wali
                    FROM mahasiswa 
                    left join dosen on (dosen.npp = mahasiswa.npp_dosen)
                WHERE nim = ?""";
        Mahasiswa mahasiswa = null;

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, nim);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                mahasiswa = new Mahasiswa(
                        rs.getString("nim"),
                        rs.getString("nama"),
                        rs.getDouble("ipk"),
                        rs.getInt("total_sks")
                );
                String npp_wali = rs.getString("npp");
                String nama_wali = rs.getString("nama_wali");
                if(npp_wali != null){
                    mahasiswa.nppDosen = new Dosen(npp_wali, nama_wali);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return mahasiswa;
    }

    @Override
    public void update(Mahasiswa mahasiswa) {
        String sql = "UPDATE mahasiswa SET nama = ?, ipk = ?, total_sks = ? ";
        if(mahasiswa.nppDosen != null){
            sql += ", npp_dosen = ? ";
        }
        sql += "WHERE nim = ?";

        try (Connection conn = KoneksiDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, mahasiswa.nama);
            pstmt.setDouble(2, mahasiswa.ipk);
            pstmt.setInt(3, mahasiswa.totalSks);
            pstmt.setString(4, mahasiswa.nim);
            if(mahasiswa.nppDosen != null){
                pstmt.setString(4, mahasiswa.nppDosen.npp);
                pstmt.setString(5, mahasiswa.nim);
            } else {
                pstmt.setString(4, mahasiswa.nim);
            }
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
}
