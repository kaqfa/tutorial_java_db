package models;

import entities.Dosen;
import entities.Mahasiswa;
import java.util.List;

public interface DosenDAO {

    void insert(Dosen dosen);
    Dosen selectByNpp(String npp);
    List<Dosen> selectAll();
    void update(Dosen dosen);
    void delete(String npp);
    List<Mahasiswa> getMahasiswaPerwalian(Dosen dosen);
    
}
