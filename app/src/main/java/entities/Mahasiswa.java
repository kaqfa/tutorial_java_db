package entities;

/**
 * Class Entity Mahasiswa
 */
public class Mahasiswa {
    public String nim;
    public String nama;
    public double ipk;
    public int totalSks;
    public Dosen nppDosen;

    public Mahasiswa(String nim, String nama, double ipk, int totalSks) {
        this.nim = nim;
        this.nama = nama;
        this.ipk = ipk;
        this.totalSks = totalSks;
    }

    @Override
    public String toString() {
        if (this.nppDosen == null)
            return "["+nim+"] "+nama+" : "+ipk+" ("+totalSks+" SKS)";
        else
            return "["+nim+"] "+nama+" : "+ipk+" ("+totalSks+" SKS) - Wali: "+nppDosen.nama;
    }
}
