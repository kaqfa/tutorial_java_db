package entities;

import java.util.ArrayList;
import java.util.List;

public class Dosen {
    public String npp;
    public String nama;
    public List<Mahasiswa> dataPerwalian;
    
    public Dosen(String npp, String nama, List<Mahasiswa> dataPerwalian) {
        this.npp = npp;
        this.nama = nama;
        this.dataPerwalian = dataPerwalian;
    }

    public Dosen(String npp, String nama) {
        this.npp = npp;
        this.nama = nama;
        this.dataPerwalian = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "["+npp+"] "+nama;
    }
}
