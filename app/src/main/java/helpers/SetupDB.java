package helpers;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class SetupDB {
    
    public static void migrate(){
        String[] sql = {"""
            CREATE TABLE IF NOT EXISTS dosen (
            npp TEXT PRIMARY KEY,
            nama TEXT NOT NULL);""",
            
            """
            CREATE TABLE IF NOT EXISTS mahasiswa (
             nim TEXT PRIMARY KEY,
             nama TEXT NOT NULL,
             ipk REAL,
             total_sks INTEGER,
             npp_dosen TEXT,
			 FOREIGN KEY (npp_dosen) REFERENCES dosen(npp)
            );""",
        };
        
        try (Connection conn = KoneksiDB.connect();
            Statement stmt = conn.createStatement()) {
                
                for(String query: sql) stmt.executeUpdate(query);
                
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
