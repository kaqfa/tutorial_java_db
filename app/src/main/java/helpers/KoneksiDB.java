package helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class KoneksiDB {
    public static Connection conn;

    private KoneksiDB(){}

    public static Connection connect() throws SQLException{
        if(conn == null || conn.isClosed()){
            conn = DriverManager.getConnection("jdbc:sqlite:mahasiswa.db");
        }

        return conn;
    }
}
